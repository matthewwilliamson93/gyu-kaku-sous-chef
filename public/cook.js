const audio = new Audio("https://dl.dropboxusercontent.com/s/1cdwpm3gca9mlo0/kick.mp3");
const onTheGrill = {};

setInterval(() => {
    const ul = document.getElementById("onTheGrill");
    ul.innerHTML = "";

    const toCook = [];
    Object.entries(onTheGrill).forEach(([key, value]) => {
        value.timeLeft--;
        if (value.timeLeft === 0) {
            delete onTheGrill[key];
            return;
        }

        toCook.push({name: value.name, timeLeft: value.timeLeft});
    });

    document.getElementById("onTheGrillHeader").style.display = toCook.length === 0 ? "none" : "block";
    toCook.sort((a, b) => {
        return a.timeLeft - b.timeLeft;
    });
    toCook.forEach((item) => {
        const li = document.createElement("li");
        li.appendChild(document.createTextNode(`${item.name} has ${item.timeLeft}s left`));
        ul.appendChild(li);
    });
}, 1000);

function reminder(item, timeToWait, flip=true) {
    audio.play().then(() => {
        audio.pause();
        audio.currentTime = 0;
    });

    onTheGrill[Date.now()] = {name: item, timeLeft: timeToWait};
    setTimeout(() => {
        audio.play();
        if (flip) {
            alert(`Flip over ${item}`);
            reminder(item, timeToWait, false);
        }
        else {
            alert(`Take off ${item}`);
        }
    }, timeToWait * 1000);
}
